#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Utility for simple UDP message passing.
"""

# Copyright (C) 2007  Felix Rabe <public@felixrabe.textdriven.com>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

from PyGTKShell.Config import _config
_config["main-loop-integrate-twisted"] = True
from PyGTKShell.RawConsole import *

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor


class Protocol(DatagramProtocol):

    def __init__(self, app):
        self.__app = app

    def datagramReceived(self, data, (host, port)):
        self.__app.receiver.print_received(data, (host, port))


class TableLayout(Table):

    def __init__(self):
        super(TableLayout, self).__init__()
        self.set_col_spacings(6)
        self.set_row_spacings(6)


class Receiver(TableLayout):

    def __init__(self, app):
        super(Receiver, self).__init__()
        self.app = app
        
        y = {"yoptions": 0}
        
        self.attach_row(MarkupLabel("<b>Receiver</b>"), **y)
        
        self.add_rows()
        self.attach_cell(LeftLabel("Local Port:"), **y)
        self.port_entry = self.attach_cell(Entry(), **y)
        self.port_entry.set_text("8753")
        
        self.add_rows()
        self.listen_button = self.attach_row(Button("Listen"), **y)
        self.listen_button.set_property("can-default", True)
        gobject.idle_add(self.listen_button.grab_default)
        self.listen_button.connect("clicked", self.__listen)
        
        self.add_rows()
        sw = self.attach_row(ScrolledWindow())
        self.textview = sw(TextViewForCode())
        self.textview.set_editable(False)
    
    def __listen(self, button):
        self.app.port = int(self.port_entry.get_text())
        self.app.proto = Protocol(self.app)
        reactor.listenUDP(self.app.port, self.app.proto)
        self.app.sender.set_sensitive(True)
        self.app.sender.send_button.grab_default()
        self.app.sender.data_entry.grab_focus()
    
    def print_received(self, data, (host, port)):
        buf = self.textview.get_buffer()
        end_it = buf.get_end_iter()
        buf.insert(end_it, "%s:%u %r\n" % (host, port, data))


class Sender(TableLayout):

    def __init__(self, app):
        super(Sender, self).__init__()
        self.app = app
        
        self.attach_row(MarkupLabel("<b>Sender</b>"))
        
        self.add_rows()
        self.attach_cell(LeftLabel("Remote IP Address:"))
        self.ip_entry = self.attach_cell(Entry())
        self.ip_entry.set_text("127.0.0.1")
        
        self.add_rows()
        self.attach_cell(LeftLabel("Remote Port:"))
        self.port_entry = self.attach_cell(Entry())
        self.port_entry.set_text("8754")
        
        self.add_rows()
        self.attach_cell(LeftLabel("Data:"))
        self.data_entry = self.attach_cell(Entry())
        self.data_entry.set_text("Hello, World!")
        
        self.add_rows()
        self.send_button = self.attach_row(Button("Send"))
        self.send_button.set_property("can-default", True)
        self.send_button.connect("clicked", self.__send)
    
    def __send(self, button):
        data = self.data_entry.get_text() + "\n"
        host = self.ip_entry.get_text()
        port = int(self.port_entry.get_text())
        self.app.proto.transport.write(data, (host, port))


class App(WindowF5RawConsoleMixin):
    
    def __init__(self):
        super(App, self).__init__()
        self.set_title("GTK UDP")
        self.set_default_size(400, -1)
        vbox = self(VBox())
        vbox.set_border_width(12)
        vbox.set_spacing(12)
        
        self.proto = None
        self.port = None
        
        self.receiver = vbox(Receiver(self))
        vbox(HSeparator(), False, False)
        self.sender = vbox(Sender(self), False, False)
        self.sender.set_sensitive(False)


def main(argv):
    App()
    main_loop_run()
    return 0


if __name__ == "__main__":
    import sys
    sys.exit(main(sys.argv))

