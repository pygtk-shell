#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The PyGTK Shell provides widgets derived from PyGTK optimized for
interactive programming as well as an interactive, extensible PyGTK console
for the rapid prototyping of GUI applications.  It can be embedded by PyGTK
applications.
"""

# Copyright (C) 2007,2009  Felix Rabe <public@felixrabe.net>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

from PyGTKShell.Core import pygtkshell_version

from distutils.core import setup

setup(
        name = "pygtk-shell",
        version = ".".join(map(str, pygtkshell_version)),
        description = "Framework for interactive GUI programming",
        long_description = __doc__.strip(),
        author = "Felix Rabe",
        author_email = "public@felixrabe.textdriven.com",
        url = "http://felixrabe.textdriven.com/pygtk-shell/",
        packages = [
            "PyGTKShell",
            ],
        scripts = ["pygtk_shell_rawconsole.py"],
        classifiers = [
            "Development Status :: 3 - Alpha",
            "Environment :: X11 Applications :: GTK",
            "Environment :: Win32 (MS Windows)",
            "Intended Audience :: Developers",
            "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
            "Operating System :: Microsoft :: Windows",
            "Operating System :: POSIX",
            "Programming Language :: Python",
            "Topic :: Software Development :: User Interfaces",
            "Topic :: Software Development :: Libraries :: Python Modules",
            "Topic :: Text Editors :: Integrated Development Environments (IDE)",
            ],
      )

