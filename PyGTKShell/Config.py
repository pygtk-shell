#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
PyGTK Shell configuration.

Use pygtkshell_config instead of globals.
"""

# Copyright (C) 2007,2009  Felix Rabe <public@felixrabe.net>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

import copy

class Config(object):
    """
    PyGTK Shell configuration class.
    """

    def __init__(self, *defaults):
        super(Config, self).__init__()
        self.__cfg = {}
        for default in defaults:
            self.__i(*default)
    
    def __i(self, key, default_value = None, flags = "",
                  pre_chg_cb_list = [], post_chg_cb_list = []):
        """
        Registers and initializes a new configuration item.
        
        pre_chg_cb_list is a list of callback functions that follow this
        signature:
            pre_chg_cb(key, old_value, new_value)
        These callback functions get called before a configuration value
        gets changed.
        
        post_chg_cb_list is a list of callback functions that follow this
        signature:
            post_chg_cb(key, new_value)
        These callback functions get called after a configuration value
        gets changed.
        """
        self.__cfg[key] = {
            "val": default_value,
            "flg": flags.lower().split(),
            "prc": copy.copy(pre_chg_cb_list),
            "poc": copy.copy(post_chg_cb_list),
        }
    
    def __getitem__(self, index):
        if not isinstance(index, basestring):
            raise TypeError, "config indices must be basestrings"
        item = self.__cfg[index]
        return item["val"]
    
    def __setitem__(self, key, value):
        item = self.__cfg[key]
        
        # Check validity:
        if "ro" in item["flg"]:
            raise TypeError, "configuration '%s' is not writable" % key
        
        if "write-true" in item["flg"] and value is not True:
            raise TypeError, (("configuration '%s' " % key) +
                              "can only be set to True")
        
        if ("write-true-once" in item["flg"] and
                (value is not True or item["val"] is not False)):
            raise TypeError, (("configuration '%s' " % key) +
                              "can only be set to True once")
        
        # Fire off notifications:
        for chg_cb in item["prc"]:
            chg_cb(key, item["val"], value)
        item["val"] = value
        for chg_cb in item["poc"]:
            chg_cb(key, value)

    def add_chg_cb(self, key, chg_cb, post = True):
        xn = {True: "poc", False: "prc"}[post]
        self.__cfg[key][xn].append(chg_cb)

    def remove_chg_cb(self, key, chg_cb, post = True):
        xn = {True: "poc", False: "prc"}[post]
        chg_cb_list = self.__cfg[key][xn]
        # Do it this way to remove the _last_ (added) change callback:
        chg_cb_list.reverse()
        chg_cb_list.remove(chg_cb)
        chg_cb_list.reverse()
    
    def keys(self):
        return self.__cfg.keys()


def _cp_pre_chg_cb_main_loop_integration(key, old_value, new_value):
    if not (old_value == False and new_value == True): return  # no-op
    head = "main-loop-integrate-"
    if not key.startswith(head):
        raise KeyError, \
            "FATAL: callback did not expect configuration key '%s'" % key
    module_desc = key[len(head):]
    _config["api-provide-" + module_desc] = True


pygtkshell_config = Config(  # alphabetically ordered defaults
    # key                           default     flags   (pre, post CBs)
    ("api-provide-cairo",           False,      "write-true"),
    ("api-provide-dbus",            False,      "write-true"),
    ("api-provide-gconf",           False,      "write-true"),
    ("api-provide-gtksourceview",   False,      "write-true"),
    ("api-provide-twisted",         False,      "write-true"),
    ("main-loop-integrate-dbus",    False,      "write-true",
        [
            _cp_pre_chg_cb_main_loop_integration,
        ],
        []),
    ("main-loop-integrate-gtk",     True,       "write-true"),
    ("main-loop-integrate-twisted", False,      "write-true",
        [
            _cp_pre_chg_cb_main_loop_integration,
        ],
        []),
    ("version",                     (1,90,8),   "ro"),
)

_config = pygtkshell_config

