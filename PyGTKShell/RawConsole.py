#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
PyGTK Shell minimal Raw Console.
"""

# Copyright (C) 2007,2009  Felix Rabe <public@felixrabe.net>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

from PyGTKShell.API import *


class RawConsoleCodeView(
        GtkTextViewUserExecMixin,
        TextViewForCode,
        ): pass

class RawConsoleOutputView(
        TextViewForCode,
        ): pass


class RawConsoleBase(VPaned):
    """
    Raw console for interactively executing Python code.
    """

    def __init__(self, *a, **kw):
        super(RawConsoleBase, self).__init__(*a, **kw)
        self.code_view = self(ScrolledWindow())(RawConsoleCodeView())
        self.code_view.textview_userexec_namespace["console"] = self
        self.output_view = self(ScrolledWindow())(RawConsoleOutputView())
        self.output_view.set_editable(False) # avoid potential confusion


class RawConsoleTracebackOutputMixin(RawConsoleBase):
    """
    Raw console writing tracebacks to output view.
    """
    
    def __init__(self, *a, **kw):
        super(RawConsoleTracebackOutputMixin, self).__init__(*a, **kw)
        buf = self.output_view.get_buffer()
        buf.create_mark("end", buf.get_end_iter(), False)
        self.__err_tag = buf.create_tag("error-tag", foreground = "#f00")
        fwf = FunctionWritableFile(self.__err_fn)
        self.code_view.textview_userexec_error_file = fwf
    
    def __err_fn(self, string):
        return self.rawconsole_err_fn(string)
    
    def rawconsole_err_fn(self, string):
        buf = self.output_view.get_buffer()
        end_it = buf.get_end_iter()
        buf.insert_with_tags(end_it, string, self.__err_tag)


class RawConsoleScrollOutputMixin(RawConsoleTracebackOutputMixin):
    """
    Raw console scrolling output view to end on traceback output.
    """
    
    def __init__(self, *a, **kw):
        super(RawConsoleScrollOutputMixin, self).__init__(*a, **kw)
        buf = self.output_view.get_buffer()
        buf.create_mark("end", buf.get_end_iter(), False)
        self.__scrolling_id = None

    def __scroll_to_end(self):
        self.__scrolling_id = None
        mark = self.output_view.get_buffer().get_mark("end")
        if not mark: return False
        self.output_view.scroll_to_mark(mark, 0.0)
        return False
    
    def rawconsole_err_fn(self, string):
        sup = super(RawConsoleScrollOutputMixin, self)
        sup.rawconsole_err_fn(string)
        if self.__scrolling_id is None:
            self.__scrolling_id = gobject.idle_add(self.__scroll_to_end)


class RawConsole(
        RawConsoleScrollOutputMixin,
        RawConsoleBase,
        ): pass


class RawConsoleCenterInitMixin(RawConsoleBase):
    """
    Raw console whose initial VPaned position is in the middle.
    """

    def __init__(self, *a, **kw):
        super(RawConsoleCenterInitMixin, self).__init__(*a, **kw)
        def f(self):
            self.set_position(self.get_allocation().height / 2)
            return False
        gobject.idle_add(f, self, priority = gobject.PRIORITY_LOW)


class RawConsoleCenterInit(
        RawConsoleCenterInitMixin,
        RawConsole,
        ): pass


class RawConsoleWithIntroMixin(RawConsoleBase):
    """
    Raw console running an example script on initialization.
    """

    def __init__(self, *a, **kw):
        super(RawConsoleWithIntroMixin, self).__init__(*a, **kw)
        buf = self.code_view.get_buffer()
        msg = a_("Press F5 or Ctrl+E to execute this code.")
        buf('# -*- coding: utf-8 -*-\n' +
            '# %s\n' % msg +
            'from PyGTKShell.API import *\n' +
            'v = console.output_view\n' +
            'o = v.get_buffer()\n' +
            'o.set_text("")\n' +
            'o(dir())\n')
        # self.code_view.do_userexec()
        # buf.select_range(*buf.get_bounds())


class RawConsoleWithIntro(
        RawConsoleCenterInitMixin,
        RawConsoleWithIntroMixin,
        RawConsole,
        ): pass


class WindowWithRawConsole(Window):
    """
    Window with a RawConsole in it and starting at a reasonable size.
    """
    
    def __init__(self, *a, **kw):
        super(WindowWithRawConsole, self).__init__(*a, **kw)
        self.set_title("PyGTK Shell RawConsole")
        self.set_default_size(400, 400)
        self.raw_console = self(RawConsoleWithIntro())


class WindowF5RawConsoleMixin(Window):
    """
    Window opening a Window containing a RawConsole when F5 is pressed.
    """
    
    def __init__(self, *a, **kw):
        super(WindowF5RawConsoleMixin, self).__init__(*a, **kw)
        self.connect("key-press-event", self.__cb_key_press_event)
    
    def __cb_key_press_event(self, window, event):
        if (KeyPressEval("F5"))(event):
            rc = WindowWithRawConsole().raw_console
            rc.code_view.textview_userexec_namespace["window"] = self
            return True
        return False


class WindowF5RawConsole(
        WindowF5RawConsoleMixin,
        Window,
        ): pass

