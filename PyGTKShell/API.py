#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
PyGTK Shell API providing simplified GObject and GTK classes.
"""

# Copyright (C) 2007,2009  Felix Rabe <public@felixrabe.net>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

# TODO: Would 'from gtk import *' make sense here?
#       Is there a more fine-grained alternative?

from PyGTKShell.Core import *
from PyGTKShell.Utilities import *
from PyGTKShell.Mixins import *


### UNMODIFIED CLASSES:

CellRenderer = gtk.CellRenderer
CellRendererText = gtk.CellRendererText
ListStore = gtk.ListStore
TreeStore = gtk.TreeStore


### ABSTRACT BASE CLASSES:

class Widget(
        GtkWidgetAutoShowMixin,
        gtk.Widget,
        ): pass

class Container(
        Widget,
        GtkContainerAddByCallingMixin,
        gtk.Container,
        ): pass

class Box(
        Container,
        GtkBoxPythonicMixin,
        gtk.Box,
        ): pass

class Paned(
        Container,
        gtk.Paned,
        ): pass

class ToolItem(
        Container,
        gtk.ToolItem,
        ): pass


### SEMI-ABSTRACT BASE CLASSES:

class Window(
        Container,
        GtkWindowFullScreenKeyMixin,
        GtkWindowLastQuitMixin,
        gtk.Window,
        ): pass

class Button(
        Widget,
        gtk.Button,
        ): pass

class Dialog(
        # Make sure there is no trace of GtkWidgetAutoShowMixin in here!
        # The dialog should only .show() when it is .run()
        GtkContainerAddByCallingMixin,
        GtkWindowLastQuitMixin,
        GtkDialogRunSimplifiedMixin,
        gtk.Dialog,
        ): pass

class FileChooserDialog(
        Dialog,
        gtk.FileChooserDialog,
        ): pass

class Label(
        Widget,
        GtkLabelUseUnderlineMixin,
        gtk.Label,
        ): pass


### SPECIFIC CLASSES:

class Alignment(
        Container,
        gtk.Alignment,
        ): pass

class CheckButton(
        Widget,
        gtk.CheckButton,
        ): pass

class DefaultButton(
        Button,
        GtkWidgetAutoDefaultMixin,
        gtk.Button,
        ): pass

class DrawingArea(
        Widget,
        gtk.DrawingArea,
        ): pass

class Entry(
        Widget,
        GtkEntryActivatesDefaultMixin,
        gtk.Entry,
        ): pass

class Expander(
        Container,
        gtk.Expander,
        ): pass

class EventBox(
        Container,
        gtk.EventBox,
        ): pass

class FileChooserButton(
        Container,
        gtk.FileChooserButton,
        ): pass

class FileChooserDialogStandard(
        FileChooserDialog,
        GtkFileChooserDialogStandardMixin,
        gtk.FileChooserDialog,
        ): pass

class Fixed(
        Container,
        gtk.Fixed,
        ): pass

class Frame(
        Container,
        gtk.Frame,
        ): pass

class HBox(
        Box,
        gtk.HBox,
        ): pass

class HPaned(
        Paned,
        gtk.HPaned,
        ): pass

class HScrollbar(
        Widget,
        gtk.HScrollbar,
        ): pass

class HSeparator(
        Widget,
        gtk.HSeparator,
        ): pass

class ImageMenuItem(
        Container,
        gtk.ImageMenuItem,
        ): pass

class LeftLabel(
        GtkMiscAlignLeftMixin,
        Label,
        ): pass

class MarkupLabel(
        GtkLabelUseMarkupMixin,
        Label,
        ): pass

class Menu(
        Container,
        gtk.Menu,
        ): pass

class MenuBar(
        Container,
        gtk.MenuBar,
        ): pass

class MenuItem(
        Container,
        gtk.MenuItem,
        ): pass

class MessageDialog(
        Dialog,
        gtk.MessageDialog,
        ): pass

class Notebook(
        Container,
        gtk.Notebook,
        ): pass

class RadioButton(
        Widget,
        gtk.RadioButton,
        ): pass

class ScrolledWindow(
        Container,
        GtkScrolledWindowPolicyAutoMixin,
        gtk.ScrolledWindow,
        ): pass

class SeparatorMenuItem(
        Container,
        gtk.SeparatorMenuItem,
        ): pass

class SpinButton(
        Widget,
        gtk.SpinButton,
        ): pass

class Statusbar(
        Container,
        gtk.Statusbar,
        ): pass

class Table(
        Container,
        GtkTableSimplifiedMixin,
        gtk.Table,
        ): pass

class TextBuffer(
        GtkTextBufferCallableMixin,
        gtk.TextBuffer,
        ): pass

if gtksourceview:
    class SourceBuffer(
        TextBuffer,
        gtksourceview.Buffer,
        ): pass

class TextView(
        Container,
        GtkTextViewAPITextBufferMixin,
        gtk.TextView,
        ): pass

_bases = (
        Container,
        GtkTextViewAPITextBufferMixin,
        GtkTextViewForCodeMixin,
        )
if gtksourceview:
    _bases += (
        GtkTextViewSourceViewMixin,
        )
_bases += (
        gtk.TextView,
        )
TextViewForCode = type("TextViewForCode", _bases, {})
del _bases

class Toolbar(
        Container,
        gtk.Toolbar,
        ): pass

class ToolButton(
        ToolItem,
        gtk.ToolButton,
        ): pass

# WARNING: gtk.Tooltips API is deprecated as of (Py)GTK 2.12 and above.
class Tooltips(
        GtkTooltipsSetTipByCallingMixin,
        gtk.Tooltips,
        ): pass

class TreeView(
        Container,
        gtk.TreeView,
        ): pass

class VBox(
        Box,
        gtk.VBox,
        ): pass

class VPaned(
        Paned,
        gtk.VPaned,
        ): pass

class VScrollbar(
        Widget,
        gtk.VScrollbar,
        ): pass

class VSeparator(
        Widget,
        gtk.VSeparator,
        ): pass

