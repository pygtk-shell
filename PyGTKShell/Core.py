#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
PyGTK Shell core.

This module imports the required and optional dependencies and checks
their versions (TODO).
"""

# Copyright (C) 2007,2009  Felix Rabe <public@felixrabe.net>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

from PyGTKShell.Config import pygtkshell_config
_config = pygtkshell_config
pygtkshell_version = _config["version"]  # DEPRECATED!

## PyGTK

import gtk
import gtk.keysyms
import gobject
import pango

## Cairo

cairo = None            # Cairo not provided
if _config["api-provide-cairo"]:
    import cairo

## DBus

dbus = None             # DBus not provided
if _config["api-provide-dbus"]:
    import dbus

## GConf

gconf = None            # GConf not provided
if _config["api-provide-gconf"]:
    import gconf

## GtkSourceView

gtksourceview = None    # GtkSourceView not provided
if _config["api-provide-gtksourceview"]:
    try:
        import gtksourceview2 as gtksourceview
    except:
        import gtksourceview

## Twisted

twisted = None          # Twisted not provided
if _config["api-provide-twisted"]:
    import twisted

if _config["main-loop-integrate-twisted"]:
    from twisted.internet import gtk2reactor
    _twisted_reactor = gtk2reactor.portableInstall()

## i18n

import gettext
# "a_" is used instead of plain "_" because Core gets * imported by API
# and API gets * imported by users.  But "_" can't be imported anyway...
a_ = gettext.translation("pygtkshell", fallback = True).gettext

## Integrated main loop support

"""
PyGTK Shell main loop support.  (TODO: move these docs into a docstring.)

You do not have to use these mechanisms if you are going to only use the
PyGTK main loop, i.e. by calling gtk.main() and gtk.main_quit().

Supported:
- PyGTK   (gtk.main)            always used
- Twisted (PortableGtkReactor)  used on demand
- TODO: DBus

Usage:
    # Boilerplate:
    from PyGTKShell.Config import _config
    _config["main-loop-integrate-dbus"]     = True  # not yet implemented
    _config["main-loop-integrate-twisted"]  = True
    from PyGTKShell.API import *

    # Set up your application here:
    w = Window()

    # Boilerplate:
    main_loop_run()
"""

def main_loop_run(*a, **kw):
    integrate_dbus      = _config["main-loop-integrate-dbus"]
    integrate_twisted   = _config["main-loop-integrate-twisted"]
    if integrate_twisted:
        _twisted_reactor.run(*a, **kw)
    else:
        gtk.main(*a, **kw)

def main_loop_quit(*a, **kw):
    integrate_dbus      = _config["main-loop-integrate-dbus"]
    integrate_twisted   = _config["main-loop-integrate-twisted"]
    if integrate_twisted:
        if not _twisted_reactor._stopped:  # Twisted is picky about this
            _twisted_reactor.stop(*a, **kw)
    else:
        gtk.main_quit(*a, **kw)

