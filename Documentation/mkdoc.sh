#!/bin/bash

DIR=$(dirname "$PWD/$0")
cd "$DIR/.."
. ./.release-info
cd "$DIR"
find "$DIR" -name "[^.]*.txt" -print -exec asciidoc -a "version=${APP_VERSION}" {} \;

