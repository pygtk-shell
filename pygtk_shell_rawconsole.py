#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
PyGTK Shell RawConsole launcher.
"""

# Copyright (C) 2007,2009  Felix Rabe <public@felixrabe.net>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

from PyGTKShell.Config import _config
# _config["main-loop-integrate-dbus"] = True
# _config["main-loop-integrate-twisted"] = True
from PyGTKShell.RawConsole import *  # imports all of API as well

def main(argv):
    WindowWithRawConsole()
    main_loop_run()  # just calling gtk.main() would work as well
    return 0

if __name__ == "__main__":
    import sys
    sys.exit(main(sys.argv))

